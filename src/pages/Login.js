import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import Logo from '../components/Logo';
import Form from '../components/Form2';
import GoogleLogin from './GoogleLoginButton';

//ERROR: a non recoverable sign in error occurred.

// import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';

export default class Login extends Component<{}> {

  constructor(props) {
    super(props);
    this.state = {showLogin: false};
  }

  signup() {
    Actions.signup()
  }

  handle() {
    GoogleSignin.signIn()
      .then((user) => {
        console.log("user123",user);
        // console.log(user.name);
        var name = user.name;
        var email = user.email;
        var username = user.givenName;
        var password = user.id;
        // console.log(name);
        // console.log(email);
        // console.log(username);
        // console.log(password);
        AsyncStorage.setItem('username', username).done();
        AsyncStorage.setItem('password', password).done();
        axios.post('http://ec2-13-127-75-64.ap-south-1.compute.amazonaws.com/Register.php',
          {
            name: name,
            email: email,
            username: username,
            password: password
          })

          .then(function (response) {
            console.log(response.data);

            if (response.data == 'exists')
              {
              axios.post('http://ec2-13-127-75-64.ap-south-1.compute.amazonaws.com/Login.php',
                {
                  username: username,
                  password: password
                })

                .then(function (response) {
                  console.log(response.data);

                  if (response.data == 'success') {
                    Actions.home();
                    this.setState({ username: username, password: password });
                  }
                  else
                    alert('Login failed');
                })

                // .catch(function (error) {
                //   console.log(error);
                //   alert('Network Error. Please try again.');
                // });
              }

            else if (response.data == 'success') {
              alert('Registeration successful');
              Actions.home();
            }

            else
              alert('Registeration failed');
          })

          .catch(function (error) {
            console.log('Registeration unsuccessful');
          });

        this.setState({ user: user });
      })
      .catch((err) => {
        console.log('WRONG SIGNIN', err);
      })
      .done();
  }


  check() {

    AsyncStorage.getItem('username').then((username) => {
      if (username)
      {
        Actions.home();
      }
      else
      {
       this.setState({showLogin: true});
      }
      this.setState({ username: username });
    })

    AsyncStorage.getItem('password').then((password) => {
      if (password)
        Actions.home();
      this.setState({ password: password });
    })

  }

  componentWillMount() {
    this.check();
  }

  render() {
    if (this.state.showLogin) 
    {
      return (
       <View style={styles.container}>
        <Logo />
        <View style={styles.signInBox}>
          </View>
            <View style={styles.signInBox}>
              <GoogleLogin 
                     onLogin={
                       (result) => {
                         console.log('Google onLogin')
                         if (result.message) {
                           alert('error: ' + result.message)
                         } else {
                           alert('Login was successful ' + result.name + ' — ' + result.email)
                           AsyncStorage.setItem('username', result.email).done();
                           Actions.home();
                         }
                      }
                     }
                     onLogout={() => alert('logged out')}
                       onError={
                         (result) => {
                           if (result.error) {
                             alert('error: ' + result.error)
                           } else {
                             alert('error')
                           }
                        }
                     }
                  />
          </View>
        </View>
      
     )
    }
    else
    {
      return(null)
    }
  }


}

const styles = StyleSheet.create({
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1c313a',
  },
  signInBox:{
    flex: 1,
  },
  signupTextCont: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',

  },
  signupText: {
    color: 'rgba(255,255,255,0.6)',
    fontSize: 16,
    fontFamily: 'Museo 500',
  },
  signupButton: {
    color: '#ffffff',
    fontSize: 20,
    fontFamily: 'Museo 900',

  },
  welcome: {
    color: '#ffffff',
    fontSize: 20,
    fontFamily: 'Museo 900',
  }
});
